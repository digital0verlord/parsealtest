package com.ibikunle.parsealtest.contract;


import com.google.firebase.database.DatabaseError;
import com.ibikunle.parsealtest.network.Actor;
import com.ibikunle.parsealtest.network.requests.CreateActorRequest;

import java.util.List;

/**
 * Created by Ibikunle Adeoluwa on 12/9/2019.
 * Cordis Corp
 * dev.ibikunle@gmail.com
 * Copyright (c) 2019 . All rights reserved.
 */
public class FirebaseContract {

    public interface CreateActorModel {

        void createActor(OnCreateActorModelCompletedListener completedListener, String agentEmail, CreateActorRequest createActorRequest);

        interface OnCreateActorModelCompletedListener {
            void onCreateActorModelSuccess(List<Actor> actorsList);

            void onCreateActorModelError(DatabaseError databaseError);
        }
    }

    public interface GetActorsModel {

        void getActors(OnGetActorsModelCompletedListener completedListener, String agentEmail);

        interface OnGetActorsModelCompletedListener {
            void onGetActorsModelSuccess(List<Actor> actorsList);

            void onGetActorsModelError(DatabaseError databaseError);
        }
    }

    public interface ListenActorsModel {

        void listen(OnListenActorsModelCompletedListener completedListener, String agentEmail);

        interface OnListenActorsModelCompletedListener {
            void onListenActorsModelSuccess(List<Actor> actorsList);
        }
    }




    public interface CreateActorView {
        void showCreateActorProgress();

        void hideCreateActorProgress();

        void createActor(List<Actor> actorsList);

        void showCreateActorError(DatabaseError databaseError);
    }

    public interface GetActorsView {
        void showGetActorsProgress();

        void hideGetActorsProgress();

        void getActors(List<Actor> actorsList);

        void showGetActorsError(DatabaseError databaseError);
    }

    public interface ListenActorsView {
        void ListenActors(List<Actor> actorsList);
    }



    public interface CreateActorPresenter {
        void onCreateActorDestroy();
    }

    public interface GetActorsPresenter {
        void onGetActorsDestroy();
    }
    public interface ListenActorsPresenter {
        void onListenActorsDestroy();
    }

}
