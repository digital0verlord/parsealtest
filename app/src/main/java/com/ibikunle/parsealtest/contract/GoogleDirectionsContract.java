package com.ibikunle.parsealtest.contract;


import com.google.android.gms.maps.model.LatLng;
import com.ibikunle.parsealtest.network.Actor;
import com.ibikunle.parsealtest.pandora.distance_route_fetcher.models.FetchDistanceResponse;

import okhttp3.ResponseBody;

/**
 * Created by Ibikunle Adeoluwa on 12/9/2019.
 * Cordis Corp
 * dev.ibikunle@gmail.com
 * Copyright (c) 2019 . All rights reserved.
 */
public class GoogleDirectionsContract {

    public interface GetDirectionsModel {

        void getDirections(OnGetDirectionsModelCompletedListener completedListener, Actor actor);

        interface OnGetDirectionsModelCompletedListener {
            void onGetDirectionsSuccess(FetchDistanceResponse fetchDistanceResponse);

            void onGetDirectionsFailure(Throwable t);

            void onGetDirectionsError(String errorBody);
        }
    }

    public interface GetDirectionsView {
        void showGetDirectionsProgress();

        void hideGetDirectionsProgress();

        void getDirections(FetchDistanceResponse fetchDistanceResponse);

        void showGetDirectionsError(String errorBody);

        void showGetDirectionsFailure(Throwable t);

    }

    public interface GetDirectionsPresenter {
        void onGetDirectionsDestroy();
    }

}
