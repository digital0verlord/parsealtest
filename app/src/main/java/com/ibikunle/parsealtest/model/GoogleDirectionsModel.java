package com.ibikunle.parsealtest.model;

import com.ibikunle.parsealtest.BuildConfig;
import com.ibikunle.parsealtest.clients.GoogleDirectionsClient;
import com.ibikunle.parsealtest.contract.GoogleDirectionsContract;
import com.ibikunle.parsealtest.network.Actor;
import com.ibikunle.parsealtest.pandora.distance_route_fetcher.fetcher.FetchDistanceService;
import com.ibikunle.parsealtest.pandora.distance_route_fetcher.models.FetchDistanceResponse;

import retrofit2.Call;
import retrofit2.Callback;


/**
 * Created by Ibikunle Adeoluwa on 12/9/2019.
 * Cordis Corp
 * dev.ibikunle@gmail.com
 * Copyright (c) 2019 . All rights reserved.
 */
public class GoogleDirectionsModel implements GoogleDirectionsContract.GetDirectionsModel {

    GoogleDirectionsClient googleDirectionsClient = new GoogleDirectionsClient();
    FetchDistanceService fetchDistanceService =
            googleDirectionsClient.getClient().create(FetchDistanceService.class);

    @Override
    public void getDirections(OnGetDirectionsModelCompletedListener completedListener, Actor actor) {
        Call<FetchDistanceResponse> call = fetchDistanceService
                .getDistanceDetails(actor.getStartLocationLat()+","+actor.getStartLocatonLng(),
                        actor.getStoptLocationLat()+","+actor.getStopLocatonLng(),
                        "driving",
                        BuildConfig.MAPS_KEY);
        call.enqueue(new Callback<FetchDistanceResponse>() {
            @Override
            public void onResponse(Call<FetchDistanceResponse> call, retrofit2.Response<FetchDistanceResponse> response) {
                if (response != null) {

                    try {
                        completedListener.onGetDirectionsSuccess(response.body());

                    } catch (Exception e) {
                        completedListener.onGetDirectionsError(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<FetchDistanceResponse> call, Throwable t) {
                completedListener.onGetDirectionsFailure(t);
            }
        });

    }
}
