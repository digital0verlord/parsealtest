package com.ibikunle.parsealtest.model;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ibikunle.parsealtest.contract.FirebaseContract;
import com.ibikunle.parsealtest.network.Actor;
import com.ibikunle.parsealtest.network.requests.CreateActorRequest;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ibikunle Adeoluwa on 12/9/2019.
 * Cordis Corp
 * dev.ibikunle@gmail.com
 * Copyright (c) 2019 . All rights reserved.
 */
@RequiresApi(api = Build.VERSION_CODES.O)
public class FirebaseModel implements FirebaseContract.CreateActorModel, FirebaseContract.GetActorsModel, FirebaseContract.ListenActorsModel {

    Instant instant = Instant.now();
    long timeStampSeconds = instant.getEpochSecond();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mDatabase = database.getReference();

    Actor actor;
    private List<Actor> actorList = new ArrayList<>();

    @Override
    public void createActor(OnCreateActorModelCompletedListener completedListener, String agentEmail, CreateActorRequest createActorRequest) {
        Map<String, Object> mHashmap = new HashMap<>();

        String email = agentEmail.trim()
                .replace("@", "")
                .replace(".", "");

        mHashmap.put(String.valueOf(timeStampSeconds)
                        .replace(":", "")
                        .replace("-", ""),
                new Actor(createActorRequest.getName(),
                        createActorRequest.getStartLocationAddress(),
                        createActorRequest.getStopLocationAddress(),
                        createActorRequest.getStartLocationLat(),
                        createActorRequest.getStartLocatonLng(),
                        createActorRequest.getStoptLocationLat(),
                        createActorRequest.getStopLocatonLng()));


        mDatabase.child(email)
                .child("actors").updateChildren(mHashmap);

        mDatabase.child(email)
                .child("actors").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    actor = postSnapshot.getValue(Actor.class);
                    actorList.add(actor);
                }

                completedListener.onCreateActorModelSuccess(actorList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                completedListener.onCreateActorModelError(databaseError);
            }
        });
    }

    @Override
    public void getActors(OnGetActorsModelCompletedListener completedListener, String agentEmail) {
        mDatabase.child(agentEmail.trim()
                .replace("@", "")
                .replace(".", ""))
                .child("actors").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                actorList.clear();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    actor = postSnapshot.getValue(Actor.class);
                    actorList.add(actor);
                }
                completedListener.onGetActorsModelSuccess(actorList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                completedListener.onGetActorsModelError(databaseError);
            }
        });
    }

    @Override
    public void listen(OnListenActorsModelCompletedListener completedListener, String agentEmail) {
        mDatabase.child(agentEmail.trim()
                .replace("@", "")
                .replace(".", ""))
                .child("actors").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    actor = postSnapshot.getValue(Actor.class);
                    actorList.add(actor);
                }

                completedListener.onListenActorsModelSuccess(actorList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
}
