package com.ibikunle.parsealtest.presenter;

import com.ibikunle.parsealtest.contract.GoogleDirectionsContract;
import com.ibikunle.parsealtest.model.GoogleDirectionsModel;
import com.ibikunle.parsealtest.network.Actor;
import com.ibikunle.parsealtest.pandora.distance_route_fetcher.models.FetchDistanceResponse;
import com.ibikunle.parsealtest.pandora.distance_route_fetcher.models.Leg;
import com.ibikunle.parsealtest.pandora.distance_route_fetcher.models.Route;

import java.util.List;

/**
 * Created by Ibkunle Adeoluwa on 9/21/2019.
 */
public class GoogleDirectionsPresenter implements GoogleDirectionsContract.GetDirectionsPresenter,
        GoogleDirectionsContract.GetDirectionsModel.OnGetDirectionsModelCompletedListener {

    private GoogleDirectionsContract.GetDirectionsView fetchView;
    private GoogleDirectionsContract.GetDirectionsModel fetchModel;
    private List<Route> routeList;
    private List<Leg> legList;


    public GoogleDirectionsPresenter(GoogleDirectionsContract.GetDirectionsView fetchView) {
        this.fetchView = fetchView;
        fetchModel = new GoogleDirectionsModel();
    }


    @Override
    public void onGetDirectionsSuccess(FetchDistanceResponse fetchDistanceResponse) {
        if (fetchView != null) {
            fetchView.hideGetDirectionsProgress();
        }
        fetchView.getDirections(fetchDistanceResponse);
    }

    @Override
    public void onGetDirectionsFailure(Throwable t) {
        fetchView.showGetDirectionsFailure(t);
        if (fetchView != null) {
            fetchView.hideGetDirectionsProgress();
        }
    }

    @Override
    public void onGetDirectionsError(String errorBody) {
        fetchView.showGetDirectionsError(errorBody);
        if (fetchView != null) {
            fetchView.hideGetDirectionsProgress();
        }
    }

    @Override
    public void onGetDirectionsDestroy() {
        this.fetchModel = null;
    }

    public void getDirections(Actor actor) {
        if (fetchModel != null) {
            fetchView.showGetDirectionsProgress();
        }
        fetchModel.getDirections(this, actor);
    }
}
