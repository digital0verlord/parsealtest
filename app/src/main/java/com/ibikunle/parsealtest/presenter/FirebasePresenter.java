package com.ibikunle.parsealtest.presenter;

import android.annotation.SuppressLint;
import android.os.Build;

import com.google.firebase.database.DatabaseError;
import com.ibikunle.parsealtest.contract.FirebaseContract;
import com.ibikunle.parsealtest.model.FirebaseModel;
import com.ibikunle.parsealtest.network.Actor;
import com.ibikunle.parsealtest.network.requests.CreateActorRequest;

import java.util.List;

import androidx.annotation.RequiresApi;

/**
 * Created by Ibikunle Adeoluwa on 12/9/2019.
 * Cordis Corp
 * dev.ibikunle@gmail.com
 * Copyright (c) 2019 . All rights reserved.
 */
public class FirebasePresenter implements FirebaseContract.CreateActorPresenter,
        FirebaseContract.GetActorsPresenter, FirebaseContract.CreateActorModel.OnCreateActorModelCompletedListener,
        FirebaseContract.GetActorsModel.OnGetActorsModelCompletedListener, FirebaseContract.ListenActorsPresenter,
        FirebaseContract.ListenActorsModel.OnListenActorsModelCompletedListener {

    private FirebaseContract.CreateActorView createActorView;
    private FirebaseContract.GetActorsView getActorsView;
    private FirebaseContract.ListenActorsView listenActorsView;


    private FirebaseContract.CreateActorModel createActorModel;
    private FirebaseContract.GetActorsModel getActorsModel;
    private FirebaseContract.ListenActorsModel listenActorModel;

    @SuppressLint("NewApi")
    public FirebasePresenter(FirebaseContract.CreateActorView createActorView) {
        this.createActorView = createActorView;
        createActorModel = new FirebaseModel();
    }

    @SuppressLint("NewApi")
    public FirebasePresenter(FirebaseContract.GetActorsView getActorsView) {
        this.getActorsView = getActorsView;
        getActorsModel = new FirebaseModel();
    }

    @SuppressLint("NewApi")
    public FirebasePresenter(FirebaseContract.GetActorsView getActorsView, FirebaseContract.ListenActorsView listenActorsView) {
        this.getActorsView = getActorsView;
        this.listenActorsView = listenActorsView;

        listenActorModel = new FirebaseModel();
        getActorsModel = new FirebaseModel();
    }


    @Override
    public void onCreateActorModelSuccess(List<Actor> actorsList) {
        createActorView.createActor(actorsList);
        if (createActorView != null) {
            createActorView.hideCreateActorProgress();
        }
    }

    @Override
    public void onListenActorsModelSuccess(List<Actor> actorsList) {
        listenActorsView.ListenActors(actorsList);
    }

    @Override
    public void onCreateActorModelError(DatabaseError databaseError) {
        createActorView.showCreateActorError(databaseError);
        if (createActorView != null) {
            createActorView.hideCreateActorProgress();
        }
    }

    @Override
    public void onGetActorsModelSuccess(List<Actor> actorsList) {
        getActorsView.getActors(actorsList);
        if (getActorsView != null) {
            getActorsView.hideGetActorsProgress();
        }
    }

    @Override
    public void onGetActorsModelError(DatabaseError databaseError) {
        getActorsView.showGetActorsError(databaseError);
        if (getActorsView != null) {
            getActorsView.hideGetActorsProgress();
        }
    }

    @Override
    public void onCreateActorDestroy() {
        this.createActorModel = null;
    }

    @Override
    public void onGetActorsDestroy() {
        this.getActorsModel = null;
    }

    @Override
    public void onListenActorsDestroy() {
        this.getActorsModel = null;
    }


    public void createActor(String email, CreateActorRequest createActorRequest) {
        if (createActorView != null) {
            createActorView.showCreateActorProgress();
        }
        createActorModel.createActor(this, email, createActorRequest);
    }

    public void getActors(String email) {
        if (getActorsView != null) {
            getActorsView.showGetActorsProgress();
        }
        getActorsModel.getActors(this, email);
    }

    public void listenActors(String email) {
        listenActorModel.listen(this, email);
    } //listen for updates

}
