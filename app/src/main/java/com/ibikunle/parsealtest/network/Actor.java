package com.ibikunle.parsealtest.network;

import lombok.Data;

/**
 * Created by Ibkunle Adeoluwa on 2/5/2020.
 */
@Data
public class Actor {
    private String id;
    private String name, startLocationAddress, stopLocationAddress;

    private Double startLocationLat, startLocatonLng, stoptLocationLat, stopLocatonLng;

    public Actor() {
    }

    public Actor(String id, String name, String startLocationAddress, String stopLocationAddress, Double startLocationLat, Double startLocatonLng, Double stoptLocationLat, Double stopLocatonLng) {
        this.id = id;
        this.name = name;
        this.startLocationAddress = startLocationAddress;
        this.stopLocationAddress = stopLocationAddress;
        this.startLocationLat = startLocationLat;
        this.startLocatonLng = startLocatonLng;
        this.stoptLocationLat = stoptLocationLat;
        this.stopLocatonLng = stopLocatonLng;
    }

    public Actor(String name, String startLocationAddress, String stopLocationAddress, Double startLocationLat, Double startLocatonLng, Double stoptLocationLat, Double stopLocatonLng) {
        this.name = name;
        this.startLocationAddress = startLocationAddress;
        this.stopLocationAddress = stopLocationAddress;
        this.startLocationLat = startLocationLat;
        this.startLocatonLng = startLocatonLng;
        this.stoptLocationLat = stoptLocationLat;
        this.stopLocatonLng = stopLocatonLng;
    }
}
