package com.ibikunle.parsealtest.network.requests;

import java.io.Serializable;

/**
 * Created by Ibkunle Adeoluwa on 9/1/2019.
 */
public class Location implements Serializable {
    private String address = null;
    private Double latitude = null, longitude = null;
    //private LatLng latLng;

//    public Location(String address, Double latitude, Double longitude, LatLng latLng) {
//        this.address = address;
//        this.latitude = latitude;
//        this.longitude = longitude;
//        this.latLng = latLng;
//    }

    public Location(String address, Double latitude, Double longitude) {
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

//    public LatLng getLatLng() {
//        return latLng;
//    }
//
//    public void setLatLng(LatLng latLng) {
//        this.latLng = latLng;
//    }


    @Override
    public String toString() {
        return "Location{" +
                "address='" + address + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
