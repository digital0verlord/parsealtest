package com.ibikunle.parsealtest.network.requests;

import lombok.Data;

/**
 * Created by Ibkunle Adeoluwa on 2/5/2020.
 */

@Data
public class CreateActorRequest {
    private String name, startLocationAddress, stopLocationAddress;

    private Double startLocationLat, startLocatonLng, stoptLocationLat, stopLocatonLng;

    public CreateActorRequest(String name, String startLocationAddress, String stopLocationAddress, Double startLocationLat, Double startLocatonLng, Double stoptLocationLat, Double stopLocatonLng) {
        this.name = name;
        this.startLocationAddress = startLocationAddress;
        this.stopLocationAddress = stopLocationAddress;
        this.startLocationLat = startLocationLat;
        this.startLocatonLng = startLocatonLng;
        this.stoptLocationLat = stoptLocationLat;
        this.stopLocatonLng = stopLocatonLng;
    }
}
