package com.ibikunle.parsealtest.clients;

import com.ibikunle.parsealtest.BuildConfig;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Ibikunle Adeoluwa on 12/21/2018.
 */

public class GoogleDirectionsClient {

    public static final String BASE_URL = BuildConfig.GOOGLE_API_ENDPOINT;
    public static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
