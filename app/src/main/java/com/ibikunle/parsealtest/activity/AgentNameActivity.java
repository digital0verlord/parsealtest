package com.ibikunle.parsealtest.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.ibikunle.parsealtest.ParsealApplication;
import com.ibikunle.parsealtest.R;
import com.ibikunle.parsealtest.pandora.Config;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AgentNameActivity extends AppCompatActivity {

    protected @BindView(R.id.email) TextInputEditText email;
    protected @BindView(R.id.emailInput) TextInputLayout emailInput;

    protected @BindView(R.id.loadingDialog) RelativeLayout loadingDialog;

    protected @BindView(R.id.confirmEmailBtn) MaterialButton confirmEmailBtn;

    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_name);

        ButterKnife.bind(this);

        initializeListeners();

        bundle = new Bundle();
    }


    @OnClick(R.id.confirmEmailBtn)
    protected void confirmEmailLogin() {
        if (isValidEmail(email, "Please enter a valid email")) {
            Config.EMAIL = email.getText().toString().trim();
            bundle.putString("email", Config.EMAIL);
            ParsealApplication.
                    navigationManager.loadActivity(this, HomeActivity.class, 0, bundle);
        }
    }


    @SuppressLint("NewApi")
    public boolean isValidEmail(EditText emailAddress, String message) {
        String value = emailAddress.getText().toString().trim();
        if (value.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(value).matches() || value.length() < 11) {
            emailInput.setError(message);
            return false;
        } else {
            emailInput.setError("");
        }
        return true;
    }

    private void initializeListeners() {
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isValidEmail(email, "Please enter a valid email");
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }
}
