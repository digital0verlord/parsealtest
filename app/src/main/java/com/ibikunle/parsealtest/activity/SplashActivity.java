package com.ibikunle.parsealtest.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.ibikunle.parsealtest.ParsealApplication;
import com.ibikunle.parsealtest.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        new Handler().postDelayed(() -> ParsealApplication
                .navigationManager.loadActivity(this, AgentNameActivity.class, 0), 3 * 1000);

    }
}
