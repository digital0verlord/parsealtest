package com.ibikunle.parsealtest.activity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DatabaseError;
import com.google.gson.Gson;
import com.ibikunle.parsealtest.BuildConfig;
import com.ibikunle.parsealtest.R;
import com.ibikunle.parsealtest.contract.FirebaseContract;
import com.ibikunle.parsealtest.fragment.ActorsFragment;
import com.ibikunle.parsealtest.fragment.MapsFragment;
import com.ibikunle.parsealtest.network.Actor;
import com.ibikunle.parsealtest.network.requests.CreateActorRequest;
import com.ibikunle.parsealtest.network.requests.Location;
import com.ibikunle.parsealtest.pandora.Config;
import com.ibikunle.parsealtest.presenter.FirebasePresenter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.paperdb.Paper;
import mumayank.com.airlocationlibrary.AirLocation;

import static android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE;
import static androidx.constraintlayout.widget.Constraints.TAG;

public class HomeActivity extends AppCompatActivity implements FirebaseContract.CreateActorView {
    final Fragment fragment2 = new ActorsFragment();
    final Fragment fragment1 = new MapsFragment();
    final FragmentManager fm = getSupportFragmentManager();

    @BindView(R.id.content) FrameLayout content;Fragment active = fragment1;
    @BindView(R.id.addActor) FloatingActionButton addActor;
    @BindView(R.id.loadingDialog) RelativeLayout loadingDialog;


    private int AUTOCOMPLETE_REQUEST_CODE = 1;
    private int LOCATION_FIELD;
    private Location startLocationCoord, stopLocationCoord, currentCoord;
    private String actorName = null, startLocationAd = null, stopLocationAd = null;
    private FirebasePresenter firebasePresenter;

    private AirLocation airLocation;
    private String myLocation = null;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                fm.beginTransaction().hide(active).show(fragment1).commit();
                active = fragment1;
                return true;
            case R.id.navigation_actors:
                fm.beginTransaction().hide(active).show(fragment2).commit();
                active = fragment2;
                return true;
        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        ButterKnife.bind(this);

        fm.beginTransaction().add(R.id.content, fragment2, "2").hide(fragment2).commit();
        fm.beginTransaction().add(R.id.content, fragment1, "1").commit();

        firebasePresenter = new FirebasePresenter(this);

        fetchMyLocation();
    }

    private void fetchMyLocation() {
            airLocation = new AirLocation(this,
                    true,
                    true, new AirLocation.Callbacks() {
                @Override
                public void onSuccess(android.location.Location location) {
                    myLocation = reverseGeoCodeLatLng(location);
                    if(myLocation != null){
                        currentCoord =
                                new Location(myLocation, location.getLatitude(), location.getLongitude());
                        Paper.book().write("myLocation", new Gson().toJson(currentCoord));
                    }
                }

                @Override
                public void onFailed(AirLocation.LocationFailedEnum locationFailedEnum) {
                    fetchMyLocation();
                }
            });
    } // get and persist my current location


    private void submitRequest() {
        firebasePresenter.createActor(Config.EMAIL,
                new CreateActorRequest(actorName, startLocationAd, stopLocationAd, startLocationCoord.getLatitude(),
                        startLocationCoord.getLongitude(), stopLocationCoord.getLatitude(), stopLocationCoord.getLongitude()));
    } //send payload to backend

    private void initPlacePicker(int location) {
        if (!Places.isInitialized()) {
            Places.initialize(this, BuildConfig.MAPS_KEY, Locale.US);
        }

        LOCATION_FIELD = location;

        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY, fields)
                .setCountry("NG")
                .build(this);
        this.startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    } //display place picker

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        airLocation.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);

                switch (LOCATION_FIELD) {
                    case 0:
                        startLocationCoord =
                                new Location(place.getName(), place.getLatLng().latitude, place.getLatLng().longitude);

                        startLocationAd = place.getName();
                        displayAddActorBotomSheet();
                        break;
                    case 1:
                        stopLocationCoord =
                                new Location(place.getName(), place.getLatLng().latitude, place.getLatLng().longitude);

                        stopLocationAd = place.getName();
                        displayAddActorBotomSheet();
                        break;
                }
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
            }
        }
    }

    @Override
    public void showCreateActorProgress() {
        loadingDialog.setVisibility(View.VISIBLE);
    } //display loading

    @Override
    public void hideCreateActorProgress() {
        loadingDialog.setVisibility(View.GONE);
    } //hide loading

    @Override
    public void createActor(List<Actor> actorsList) {
        Paper.book().write("actorList", new Gson().toJson(actorsList));
    } //persist data

    @Override
    public void showCreateActorError(DatabaseError databaseError) {
        showToast("Error adding actor "+databaseError.getMessage());
    } //display error

    @OnClick(R.id.addActor)
    protected void displayAddActorBotomSheet() {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this, R.style.DialogStyle);
        mBottomSheetDialog.setCancelable(false);
        View sheetView = getLayoutInflater().inflate(R.layout.add_actor_dialog, null);
        MaterialButton cancel, submit;
        EditText name;
        TextView startLocation, stopLocation;
        cancel = sheetView.findViewById(R.id.cancel);
        submit = sheetView.findViewById(R.id.submit);
        stopLocation = sheetView.findViewById(R.id.stopLocation);
        startLocation = sheetView.findViewById(R.id.startLocation);
        name = sheetView.findViewById(R.id.name);

        Objects.requireNonNull(mBottomSheetDialog.getWindow())
                .setSoftInputMode(SOFT_INPUT_STATE_VISIBLE);

        cancel.setOnClickListener(view -> mBottomSheetDialog.dismiss());
        if (actorName != null) {
            name.setText(actorName);
        }
        if (startLocationAd != null) {
            startLocation.setText(startLocationAd);
        }
        if (stopLocationAd != null) {
            stopLocation.setText(stopLocationAd);
        }

        startLocation.setOnClickListener(v -> {
            actorName = name.getText().toString().trim();

            mBottomSheetDialog.dismiss();

            initPlacePicker(0);
        });

        stopLocation.setOnClickListener(v -> {
            actorName = name.getText().toString().trim();

            mBottomSheetDialog.dismiss();

            initPlacePicker(1);
        });

        submit.setOnClickListener(v -> {
            mBottomSheetDialog.dismiss();

            if(actorName != null && startLocationAd != null && stopLocationAd != null){
                submitRequest();
            }else{
                showToast("Error One or more values are missing");
            }
        });


        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setOnShowListener(dialog -> {
            BottomSheetDialog d = (BottomSheetDialog) dialog;
            View bottomSheetInternal = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
            BottomSheetBehavior.from(bottomSheetInternal).setState(BottomSheetBehavior.STATE_EXPANDED);
        });
        mBottomSheetDialog.show();
    } //display actor creator

    public void showToast(final String msg) {
        runOnUiThread(() -> Toast.makeText(HomeActivity.this, msg, Toast.LENGTH_SHORT).show());
    } //display toast

    public String reverseGeoCodeLatLng(android.location.Location location) {
        String currentLocation = "";
        Geocoder geoCoder = new Geocoder(this, Locale.getDefault());
        List<Address> list = null;
        try {
            list = geoCoder.getFromLocation(location.getLatitude(),location.getLongitude(), 1);
            if (list != null & list.size() > 0) {
                Address address = list.get(0);
                currentLocation = address.getAddressLine(0);
            }
        } catch (IOException e) {

        }
        return currentLocation;
    } //do reverse location loockup

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        airLocation.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        firebasePresenter.onCreateActorDestroy();
    }
}
