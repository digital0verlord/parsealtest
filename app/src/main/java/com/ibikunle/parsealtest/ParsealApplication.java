package com.ibikunle.parsealtest;

import android.app.Application;

import com.ibikunle.parsealtest.pandora.Config;
import com.ibikunle.parsealtest.pandora.ConnectionManager;
import com.ibikunle.parsealtest.pandora.NavigationManager;
import com.ibikunle.parsealtest.pandora.SharedPreferenceManager;

import io.paperdb.Paper;


/**
 * Created by Ibkunle Adeoluwa on 12/27/2018.
 */


public class ParsealApplication extends Application {

    private static final String TAG = ParsealApplication.class.getSimpleName();

    public static NavigationManager navigationManager;
    public static SharedPreferenceManager sharedPreferenceManager;
    public static ConnectionManager connectionManager;
    public static Config config;


    @Override
    public void onCreate() {
        super.onCreate();

        navigationManager = new NavigationManager();
        sharedPreferenceManager = new SharedPreferenceManager(this);
        connectionManager = new ConnectionManager(this);
        Paper.init(this);
        config = new Config(this);

    }


}