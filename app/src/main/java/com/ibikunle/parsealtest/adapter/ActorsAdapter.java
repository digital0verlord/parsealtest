package com.ibikunle.parsealtest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ibikunle.parsealtest.R;
import com.ibikunle.parsealtest.network.Actor;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 */
public class ActorsAdapter extends RecyclerView.Adapter<ActorsAdapter.MyViewHolder> {
    private Context mContext;
    List<Actor> actorsList;
    Actor actors;


    public ActorsAdapter(Context mContext, List<Actor> actorsList) {
        this.mContext = mContext;
        this.actorsList = actorsList;
    }


    @NonNull
    @Override
    public ActorsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.actor_item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ActorsAdapter.MyViewHolder holder, int position) {
        actors = actorsList.get(position);
        holder.actorName.setText(actors.getName());
        holder.start.setText(actors.getStartLocationAddress());
        holder.stop.setText(actors.getStopLocationAddress());
    }

    @Override
    public int getItemCount() {
        return actorsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder  {

        protected @BindView(R.id.actorName) TextView actorName;
        protected @BindView(R.id.start) TextView start;
        protected @BindView(R.id.stop) TextView stop;
        protected @BindView(R.id.card_view) CardView card_view;

        public MyViewHolder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }



}
