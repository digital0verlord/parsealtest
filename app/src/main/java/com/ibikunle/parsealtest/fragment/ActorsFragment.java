package com.ibikunle.parsealtest.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.google.firebase.database.DatabaseError;
import com.google.gson.Gson;
import com.ibikunle.parsealtest.R;
import com.ibikunle.parsealtest.activity.HomeActivity;
import com.ibikunle.parsealtest.adapter.ActorsAdapter;
import com.ibikunle.parsealtest.contract.FirebaseContract;
import com.ibikunle.parsealtest.network.Actor;
import com.ibikunle.parsealtest.pandora.Config;
import com.ibikunle.parsealtest.presenter.FirebasePresenter;

import java.util.List;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;

/**
 * A simple {@link Fragment} subclass.
 */
public class ActorsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, FirebaseContract.GetActorsView {
    View view;

    protected @BindView(R.id.recycler) ShimmerRecyclerView recycler;
    protected @BindView(R.id.swipeRefresh) SwipeRefreshLayout swipeRefresh;
    private FirebasePresenter firebasePresenter;
    private String actorList = null;
    private List<Actor> actors;
    private ActorsAdapter actorsAdapter;

    @BindView(R.id.errorText) TextView errorText;


    public ActorsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_actors, container, false);

        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefresh.setOnRefreshListener(this);
        firebasePresenter = new FirebasePresenter(this);

        actorList = Paper.book().read("actorList");

        inflateActorLists(actorList);
    }

    private void inflateActorLists(String actorList) {
        try{
            if(actorList != null  && !actorList.isEmpty()
                    && !actorList.equals("null")){
                actors = (List<Actor>) new Gson().fromJson(actorList, Actor.class);
                renderActors(actors);
            }else{
                firebasePresenter.getActors(Config.EMAIL);
            }
        }catch (Exception e){
            firebasePresenter.getActors(Config.EMAIL);
        }

    }//load actors from local storage

    private void renderActors(List<Actor> actors){
        if (actors != null && actors.size() > 0) {
            Paper.book().write("actorList", new Gson().toJson(actors));

            recycler.setItemAnimator(new DefaultItemAnimator());
            recycler.setLayoutManager(new LinearLayoutManager(getContext()));
            recycler.addItemDecoration(new DividerItemDecoration(getContext(),LinearLayoutManager.VERTICAL));
            actorsAdapter = new ActorsAdapter(getActivity().getApplicationContext(), actors);
            recycler.setAdapter(actorsAdapter);
        }else{
            errorText.setVisibility(View.VISIBLE);
        }
    } // render list

    @Override
    public void onRefresh() {
        firebasePresenter.getActors(Config.EMAIL);
    } //refresh actors

    @Override
    public void showGetActorsProgress() {
        errorText.setVisibility(View.GONE);
        swipeRefresh.setRefreshing(true);
    } // show swipe refresh

    @Override
    public void hideGetActorsProgress() {
        swipeRefresh.setRefreshing(false);
    } //hide swipe refresh

    @Override
    public void getActors(List<Actor> actorsList) {
        renderActors(actorsList);
    } //get actors

    @Override
    public void showGetActorsError(DatabaseError databaseError) {
        showToast("Error getting actors "+databaseError.getMessage());
    } //on get actors error

    public void showToast(final String msg) {
        getActivity().runOnUiThread(() -> Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show());
    } //display toast

    @Override
    public void onDestroy() {
        super.onDestroy();
        firebasePresenter.onGetActorsDestroy();
    }
}

