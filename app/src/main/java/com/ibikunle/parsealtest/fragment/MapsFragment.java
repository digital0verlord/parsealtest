package com.ibikunle.parsealtest.fragment;


import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DatabaseError;
import com.ibikunle.parsealtest.R;
import com.ibikunle.parsealtest.contract.FirebaseContract;
import com.ibikunle.parsealtest.contract.GoogleDirectionsContract;
import com.ibikunle.parsealtest.network.Actor;
import com.ibikunle.parsealtest.pandora.Config;
import com.ibikunle.parsealtest.pandora.distance_route_fetcher.models.FetchDistanceResponse;
import com.ibikunle.parsealtest.pandora.distance_route_fetcher.models.Route;
import com.ibikunle.parsealtest.pandora.mapper.GetPathFromLocation;
import com.ibikunle.parsealtest.pandora.mapper.animator.GoogleMarkerAnim;
import com.ibikunle.parsealtest.pandora.mapper.animator.LatLngInterpolator;
import com.ibikunle.parsealtest.pandora.mapper.animator.MarkerAnimation;
import com.ibikunle.parsealtest.presenter.FirebasePresenter;
import com.ibikunle.parsealtest.presenter.GoogleDirectionsPresenter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.graphics.Paint.Join.ROUND;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapsFragment extends Fragment implements OnMapReadyCallback, FirebaseContract.GetActorsView,
        FirebaseContract.ListenActorsView, GoogleDirectionsContract.GetDirectionsView {

    protected @BindView(R.id.reanimate) FloatingActionButton reanimate;
    View view;
    private GoogleMap mMap;
    private FirebasePresenter firebasePresenter;
    private GoogleDirectionsPresenter googleDirectionsPresenter;
    private boolean mapReady = false;
    private ArrayList<MarkerOptions> myMarkers = new ArrayList<>();
    private MarkerOptions m = new MarkerOptions();
    private List<LatLng> polyLineList;
    private ArrayList<LatLng> polList;
    Actor myActor;

    public MapsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_maps, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        polyLineList = new ArrayList<>();
        polList =  new ArrayList<>();

        firebasePresenter = new FirebasePresenter(this, this);
        googleDirectionsPresenter = new GoogleDirectionsPresenter(this);

        refresh();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (isDayTime()) {
            googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.day_style_json));
        } else {
            googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.night_style_json));
        }

        mapReady = true;


        LatLng etiOsa = new LatLng(6.4391115, 3.46908);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(etiOsa)
                .zoom(15)
                .bearing(90)
                .tilt(30)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }


    private boolean isDayTime() {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if (timeOfDay >= 0 && timeOfDay < 16)
            return true;
        return false;
    } //check day nigh cycle and render different map modes


    @Override
    public void showGetActorsProgress() {

    } //display progress for actors

    @Override
    public void hideGetActorsProgress() {

    } //hie progress for actors

    @Override
    public void getActors(List<Actor> actorsList) {
        renderActorRoutes(actorsList);
    } //get all actors

    @OnClick(R.id.reanimate)
    protected void refresh() {
        if (mapReady && mMap != null) {
            mMap.clear();
        }
        firebasePresenter.getActors(Config.EMAIL);
        firebasePresenter.listenActors(Config.EMAIL);
    } //clear map and reload points

    @Override
    public void showGetActorsError(DatabaseError databaseError) {

    } //actor load error

    @Override
    public void ListenActors(List<Actor> actorsList) {
        renderActorRoutes(actorsList);
        try {
            resizeCamera();
        } catch (IllegalStateException e) {

        }
    } //listen for new routes

    private void resizeCamera() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (MarkerOptions marker : myMarkers) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();

        int padding = 0; // offset from edges of the map in pixels
        CameraUpdate c = CameraUpdateFactory.newLatLngBounds(bounds, padding);

        mMap.moveCamera(c);
        mMap.animateCamera(c);
    } //recenter camera

    @SuppressLint("NewApi")
    private void renderActorRoutes(List<Actor> actorsList) {
        if (mapReady) {
            actorsList.forEach((actor) -> displayMapRoutesandMakers(actor));
        }
    } //draw map routes and markers

    private void displayMapRoutesandMakers(Actor actor) {
        myActor = actor;
        LatLng start, end;
        start = new LatLng(actor.getStartLocationLat(), actor.getStartLocatonLng());
        end = new LatLng(actor.getStoptLocationLat(), actor.getStopLocatonLng());


        new GetPathFromLocation(start, end, polyLine -> mMap.addPolyline(polyLine)).execute();


        drawStartStopMarker(m, start, actor.getStartLocationAddress(), BitmapDescriptorFactory.fromResource(R.drawable.start));

        drawStartStopMarker(m, end, actor.getStopLocationAddress(), BitmapDescriptorFactory.fromResource(R.drawable.finish));

        googleDirectionsPresenter.getDirections(actor);
    }

    private void drawStartStopMarker(MarkerOptions options, LatLng latLng, String locationName, BitmapDescriptor icon) {
        options = new MarkerOptions();
        options.position(latLng);
        options.title(locationName);
        options.icon(icon);
        myMarkers.add(options);
        mMap.addMarker(options).showInfoWindow();
    }

    @Override
    public void showGetDirectionsProgress() {

    }

    @Override
    public void hideGetDirectionsProgress() {

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void getDirections(FetchDistanceResponse fetchDistanceResponse) {
        List<Route> routeList = fetchDistanceResponse.getRoutes();
        for (Route route : routeList) {
            String polyLine = route.getOverviewPolyline().getPoints();
            polyLineList = decodePoly(polyLine);

            LatLng actors = new LatLng(myActor.getStartLocationLat(), myActor.getStartLocatonLng());

            Marker marker = mMap
                    .addMarker(new MarkerOptions()
                    .position(actors)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.auto)));

            polyLineList.forEach((LatLng) -> polList.add(LatLng));

            new GoogleMarkerAnim().animateLine(polList,mMap,marker,getContext());
        }
    }

    @Override
    public void showGetDirectionsError(String errorBody) {

    }

    @Override
    public void showGetDirectionsFailure(Throwable t) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        googleDirectionsPresenter.onGetDirectionsDestroy();
        firebasePresenter.onGetActorsDestroy();
        firebasePresenter.onListenActorsDestroy();
    }


    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

}
