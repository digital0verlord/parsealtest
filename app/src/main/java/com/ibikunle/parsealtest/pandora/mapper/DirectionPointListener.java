package com.ibikunle.parsealtest.pandora.mapper;

import com.google.android.gms.maps.model.PolylineOptions;

/**
 * Created by Ibikunle Adeoluwa on 2/6/2020.
 * Cordis Corp
 * dev.ibikunle@gmail.com
 * Copyright (c) 2020 . All rights reserved.
 */
public interface DirectionPointListener {
    public void onPath(PolylineOptions polyLine);
}
