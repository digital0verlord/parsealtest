package com.ibikunle.parsealtest.pandora;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;


/**
 * Created by Ibikunle Adeoluwa on 1/4/2019.
 */
public class SharedPreferenceManager {
    private static final String PREFS_NAME = Config.SHARED_PREF_NAME;
    protected static final String UTF8 = "utf-8";
    private static final char[] SECURE = {'a', 's', 'e', 'n', 'd', 'o'};
    private Context context;

    public SharedPreferenceManager(Context context) {
        this.context = context;
    }

    /**
     * @param context
     * @param key
     * @param value
     * @return
     */
    public static boolean saveToPreference(Context context, String key, String value) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        return editor.commit();
    }


    /**
     * @param context
     * @param key
     * @return
     */
    public static String loadFromPreference(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return settings.getString(key, "");
    }

    /**
     * @param context
     * @param key
     * @param value
     * @return
     */
    public static Boolean saveBooleanToPrefs(Context context, String key, Boolean value) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        return editor.commit();
    }

    /**
     * @param context
     * @param key
     * @return
     */
    public static Boolean loadBooleanFromPreference(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return settings.getBoolean(key, true);
    }


    public static String loadEncryptedString(Context context, String key) {
        String decryptedString = null;
        try {
            SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
            String value = settings.getString(key, "");
            final byte[] bytes = value != null ? Base64.decode(value, Base64.DEFAULT) : new byte[0];
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
            SecretKey enckey = keyFactory.generateSecret(new PBEKeySpec(SECURE));
            Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
            pbeCipher.init(Cipher.DECRYPT_MODE, enckey, new PBEParameterSpec(Settings.Secure.getString(context.getContentResolver(), Settings.System.ANDROID_ID).getBytes(UTF8), 20));
            decryptedString = new String(pbeCipher.doFinal(bytes), UTF8);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return decryptedString;
    }


    public static void saveEncryptedStringToPreference(Context context, String key, String value) {
        try {
            final byte[] bytes = value != null ? value.getBytes(UTF8) : new byte[0];
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
            SecretKey enckey = keyFactory.generateSecret(new PBEKeySpec(SECURE));
            Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
            pbeCipher.init(Cipher.ENCRYPT_MODE, enckey,
                    new PBEParameterSpec(Settings.Secure.getString(context.getContentResolver(),
                            Settings.System.ANDROID_ID).getBytes(UTF8), 20));

            SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(key, new String(Base64.encode(pbeCipher.doFinal(bytes), Base64.NO_WRAP), UTF8));
            editor.commit();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
