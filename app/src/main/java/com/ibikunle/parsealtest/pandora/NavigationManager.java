package com.ibikunle.parsealtest.pandora;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.ibikunle.parsealtest.R;


/**
 * Created by Ibikunle Adeoluwa on 12/21/2018.
 */
public class NavigationManager {

    /**
     * @param activity
     * @param activityClass
     */
    public void loadActivity(Activity activity, Class activityClass, int direction) {
        Intent i = new Intent(activity, activityClass);
        activity.startActivity(i);
        switch (direction) {
            case 0: //forward
                activity.overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
                activity.finish();
                break;
            case 1: //backward
                activity.overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right);
                activity.finish();
                break;
        }
    }

    public void loadSplashActivity(Activity activity, Class activityClass, int delay) {
        Intent i = new Intent(activity, activityClass);
        activity.startActivity(i);

        new Handler().postDelayed(() -> {
            activity.overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
            activity.finish();

        }, delay * 1000); // wait for 5 seconds

    }


    /**
     * @param currentActivity
     * @param newActivity
     * @param direction
     * @param bundle
     */
    public void loadActivity(Activity currentActivity, Class newActivity, int direction, Bundle bundle) {
        Intent i = new Intent(currentActivity, newActivity);
        i.putExtras(bundle);
        currentActivity.startActivity(i);
        switch (direction) {
            case 0:
                currentActivity.overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
                currentActivity.finish();
                break;
            case 1:
                currentActivity.overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right);
                currentActivity.finish();
                break;
        }
    }

    /**
     * @param fragmentClass
     */
    public void loadFragment(Fragment fragmentClass, Context mContext, Bundle bundle, int direction) {
        AppCompatActivity activity = (AppCompatActivity) mContext;
        fragmentClass.setArguments(bundle);
        switch (direction) {
            case 0:
                activity.getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
                        .replace(R.id.content, fragmentClass)
                        .addToBackStack(null)
                        .commit();
                break;
            case 1:
                activity.getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right)
                        .replace(R.id.content, fragmentClass)
                        .addToBackStack(null)
                        .commit();
                break;
        }

    }

    public void loadInternalFragment(Fragment fragmentClass, View viewHolder, Context mContext, Bundle bundle, int direction) {
        AppCompatActivity activity = (AppCompatActivity) mContext;
        fragmentClass.setArguments(bundle);
        switch (direction) {
            case 0:
                activity.getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
                        .replace(viewHolder.getId(), fragmentClass)
                        .addToBackStack(null)
                        .commit();
                break;
            case 1:
                activity.getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_in_right)
                        .replace(viewHolder.getId(), fragmentClass)
                        .addToBackStack(null)
                        .commit();
                break;
        }

    }


    public void loadBrowserActivity(String url, Context mContext) {
        AppCompatActivity activity = (AppCompatActivity) mContext;
        Intent openUrlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        if (openUrlIntent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivity(openUrlIntent);
        }
    }


    /**
     * @param view
     */
    public void slideViewUpFromBottom(View view, View hidingView) {
        hidingView.setVisibility(View.GONE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(300);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    /**
     * @param view
     */
    public void slideViewDownFromTop(View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }

    /**
     * @param view
     */
    public void slideViewLeftFromRight(View view) {
        // view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                view.getWidth(),                 // fromXDelta
                0,                 // toXDelta
                0,  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    /**
     * @param view
     */
    public void slideViewRightFromLeft(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                view.getWidth(),                 // toXDelta
                0,  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    public void loadNewApplication(Context mContext, String packageName) {
        AppCompatActivity activity = (AppCompatActivity) mContext;
        Intent launchIntent = activity.getPackageManager()
                .getLaunchIntentForPackage(packageName);
        if (launchIntent != null) {
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(launchIntent);
        } else {
            // Bring user to the market or let them choose an app?
            launchIntent = new Intent(Intent.ACTION_VIEW);
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            launchIntent.setData(Uri.parse("market://details?id=" + packageName));
            activity.startActivity(launchIntent);
        }
    }

    public void shareOnWhatsapp(Context context, String message) {
        AppCompatActivity activity = (AppCompatActivity) context;
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, message);
        try {
            activity.startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            loadNewApplication(context, "com.whatsapp");
        }
    }

    @SuppressLint("NewApi")
    public void sendMessageToDriver(Activity activity, String phone) {
        String formattedNumber = PhoneNumberUtils.formatNumber(phone);
        try {
            Intent sendIntent = new Intent("android.intent.action.MAIN");
            sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Hello , Hello im a member of the group you are assigned to drive");
            sendIntent.putExtra("jid", formattedNumber + "@s.whatsapp.net");
            sendIntent.setPackage("com.whatsapp");
            activity.startActivity(sendIntent);
        } catch (Exception e) {
            Toast.makeText(activity, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

}
