package com.ibikunle.parsealtest.pandora.mapper.animator;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.content.Context;
import android.util.Property;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;

/**
 * Created by Ibkunle Adeoluwa on 2/6/2020.
 */
public class GoogleMarkerAnim {
    static GoogleMap map;
    ArrayList<LatLng> _trips = new ArrayList<>();
    Marker _marker;
    LatLngInterpolator _latLngInterpolator = new LatLngInterpolator.Spherical();

    public void animateLine(ArrayList<LatLng> Trips, GoogleMap map, Marker marker, Context current) {
        _trips.addAll(Trips);
        _marker = marker;

        animateMarker();
    }


    public void animateMarker() {
        TypeEvaluator<LatLng> typeEvaluator = (fraction, startValue, endValue) -> _latLngInterpolator.interpolate(fraction, startValue, endValue);
        Property<Marker, LatLng> property = Property.of(Marker.class, LatLng.class, "position");

        ObjectAnimator animator = ObjectAnimator.ofObject(_marker, property, typeEvaluator, _trips.get(0));

        //ObjectAnimator animator = ObjectAnimator.o(view, "alpha", 0.0f);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationCancel(Animator animation) {
                //  animDrawable.stop();
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                //  animDrawable.stop();
            }

            @Override
            public void onAnimationStart(Animator animation) {
                //  animDrawable.stop();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                //  animDrawable.stop();
                if (_trips.size() > 1) {
                    _trips.remove(0);
                    animateMarker();
                }
            }
        });

        animator.setDuration(300);
        animator.start();
    }
}
