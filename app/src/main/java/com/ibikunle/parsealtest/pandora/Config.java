package com.ibikunle.parsealtest.pandora;


import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class Config {

    public static String SHARED_PREF_NAME = "ParsealAppPreference";
    public static Boolean LOGGER = true;
    public static Boolean SEARCHING = true;

    public static String FirstName = "";
    public static int ONBOARD_APP_REQUEST_CODE = 99;
    private Context context;
    public static String PAYMENT_METHOD =  "";
    public static String EMAIL = null;
    public static String TOKEN = null;
    public static String UID = null;


    /**
     * Connection
     */
    public long startTime;
    public long endTime;
    public long fileSize;
    public OkHttpClient client = new OkHttpClient();

    public int POOR_BANDWIDTH = 150;
    public int AVERAGE_BANDWIDTH = 550;
    public int GOOD_BANDWIDTH = 2000;

    public int delay = 10000;

    public Request request = new Request.Builder()
            .url("https://sheeft.com.ng/wp-content/uploads/2019/07/img2-1.jpg")
            .build();


    public Config(Context context) {
        this.context = context;
    }
    /**
     * '
     *
     * @param TAG
     * @param message
     */
    public static void logDebug(String TAG, String message) {
        if (LOGGER.equals(true)) {
            Log.d(TAG, "======> \t" + message);
        }
    }

    /**
     * @param TAG
     * @param message
     * @param exception
     */
    public static void logError(String TAG, String message, Exception exception) {
        if (LOGGER.equals(true)) {
            logDebug(TAG, "======> \t" + message);
        }
        /*FirebaseCrash.logcat(Log.ERROR, TAG, message);
        FirebaseCrash.report(exception);*/
    }

    /**
     * @param TAG
     * @param message
     */
    public static void logError(String TAG, String message) {
        if (LOGGER.equals(true)) {
            logDebug(TAG, "======> \t" + message);
        }
        /*FirebaseCrash.logcat(Log.ERROR, TAG, message);*/
    }

    /**
     * @param TAG
     * @param message
     */
    public static void logInfo(String TAG, String message) {
        if (LOGGER.equals(true)) {
            Log.i(TAG, "======> \t" + message);
        }
    }
    /**
     * Converts text to a hashed string
     *
     * @param text
     * @return
     */
    public static String HashString(String text) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(text.trim().getBytes("UTF-8"));
            Log.i("CONFIG", "HASHING FILE = " + ToBase64Encode(hash));
            return ToBase64Encode(hash);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        } catch (UnsupportedEncodingException e) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, e);
            return "";
        }
    }

    /**
     * Converts an original text to base 64 string
     *
     * @param originalInput
     * @return
     */
    public static String ToBase64Encode(String originalInput) {
        try {
            return new String(Base64.encodeToString(originalInput.getBytes("UTF-8"),  Base64.NO_PADDING | Base64.NO_WRAP));
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    public static String ToBase64Encode(byte[] originalInput) {
        return new String(Base64.encodeToString(originalInput, Base64.NO_PADDING | Base64.NO_WRAP ));
    }

    /**
     * Converts an bas64 text to original text
     *
     * @param base64Text
     * @return
     */
    public static String FromBase64Decode(String base64Text) {
        return new String(Base64.decode(base64Text, Base64.NO_PADDING));
    }

    /**
     * Gets the android Secure Id
     *
     * @param context
     * @return
     */
    public static String getAndroidId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }


    /**
     * Copys a text item to the clipboad to be used
     *
     * @param context
     * @param key
     * @param text
     */
    public static void copyToClipBoard(Context context, String key, String text) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(key, text);
        if (clipboard == null) return;
        clipboard.setPrimaryClip(clip);
    }

    /**
     * Converts a hash map to url encoded string format
     *
     * @param params
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String getDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        Log.d("Config", "getDataString: " + result.toString());
        return result.toString();
    }

    /**
     * Saves a String to Android Local Store
     *
     * @param context
     * @param data
     */
    public static void saveStringToLocal(Context context, String data) {
        SharedPreferences prefs = context.getSharedPreferences(
                "com.almondmedia.myapplication", Context.MODE_PRIVATE);

        prefs.edit().putString("tkn", data).apply();
    }

    /**
     * Override method Saves a String to Android Local Store
     *
     * @param context
     * @param key
     * @param data
     */
    public static void saveStringToLocal(Context context, String key, String data) {
        SharedPreferences prefs = context.getSharedPreferences(
                "com.almondmedia.myapplication", Context.MODE_PRIVATE);

        prefs.edit().putString(key, data).apply();
    }

    /**
     * Gets the String from android local store
     *
     * @param context
     * @param key
     * @return
     */
    public static String getStringFromLocal(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(
                "com.almondmedia.myapplication", Context.MODE_PRIVATE);
        return prefs.getString(key, "");
    }

    public static boolean shitTalkerValidation(String word) {
        ArrayList<String> badWords = new ArrayList<>();
        badWords.add("kidnap");
        badWords.add("kidnapper");
        badWords.add("kidnappers");
        badWords.add("kidnapping");
        badWords.add("thief");
        badWords.add("rob");
        badWords.add("steal");
        badWords.add("gang");
        badWords.add("cult");
        badWords.add("cultists");
        badWords.add("sacrifice");
        badWords.add("thieves");
        badWords.add("fuck");
        badWords.add("pussy");
        badWords.add("dick");
        badWords.add("fuckers");
        badWords.add("paedophile");
        badWords.add("ass");
        badWords.add("shit");
        badWords.add("bitch");
        badWords.add("cunt");
        badWords.add("robbers");
        badWords.add("armed");
        badWords.add("gun");
        badWords.add("bloody");
        badWords.add("blood");
        badWords.add("fuck");
        badWords.add("sex");
        badWords.add("wanker");
        badWords.add("twat");
        badWords.add("rape");
        badWords.add("rapist");
        for (int i = 0; i < badWords.size(); i++) {
            String badWord = badWords.get(i);
            if (word.toLowerCase().contains(badWord)) {
                return true;
            }
        }
        return false;
    }

    /**
     * method to Hide keyboard
     * @param view
     */
    public void hideKeyboardFrom(View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
}
